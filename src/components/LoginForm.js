import { Form, Button } from 'react-bootstrap'
import { useState, useContext, useEffect } from 'react'
import { ApplicationContext } from '../contexts/ApplicationContext'
import RegisterForm from './RegisterForm'

export default function LoginForm({ setIsRedirect }) {

	const { setUser } = useContext(ApplicationContext)

	const [ credentials, setCredentials ] = useState({
		email: "",
		password: ""
	})

	const [show, setShow] = useState(false);

	useEffect (()=>{
		console.log(show)
	},[show])

	const handleOpen = () => setShow(true);

	const handleClose = () => setShow(false);

	const [ isLoading, setIsLoading ] =useState(false)

	const handleSubmit = e =>{
		e.preventDefault()
		setIsLoading(true)
		fetch('https://quiet-hollows-90585.herokuapp.com/api/users/login',{
			method: "POST",
			body: JSON.stringify(credentials),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then( res => {
			console.log(res)
			setIsLoading(false)
			if (res.status === 200) {
				alert("Login successful")
				return res.json()
			} else {
				alert("Invalid Credentials")
				throw new Error("Invalid Credentials")
			}
		})
		.then( token => {
			let access = token.token

			localStorage.setItem("token", access)
			// console.log(access)
			return fetch('https://quiet-hollows-90585.herokuapp.com/api/users',{
				headers : {
					'Authorization' : `Bearer ${access}`
				}
			})
		})
		.then( res => res.json())
		.then( data =>{
			// console.log(data)
			const { firstName, lastName, email, isAdmin} = data
			setUser({
				userId: data._id,
				firstName,
				lastName,
				email,
				isAdmin
			})
			setIsRedirect(true)
		})
		.catch( err => console.log(err))
	}

	const handleChange = e =>{
		setCredentials({
			...credentials,
			[e.target.id] : e.target.value
		})
	}

	return(
		<div>
			<Form onSubmit={handleSubmit} id="form">

				<Form.Group controlId="email">
					<Form.Control 
						type="email"
						onChange={handleChange}
						value={credentials.email}
						placeholder="Email"
					/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Control 
						type="password" 
						onChange={handleChange}
						value={credentials.password}
						placeholder="Password"
					/>
				</Form.Group>

				{ 	isLoading ?
						<Button type="submit" disabled>Login</Button>
					:   <Button type="submit" >Login</Button>
				}
			
				<div className="mikol"></div>
				<div>
					<p className="mt-1">No account yet?</p>
					{/* <Button type="submit" variant="info" block>Register</Button> */}
					<Button variant="info" block onClick={handleOpen}>
						Register
						<RegisterForm handleClose={handleClose} show={show} />
					</Button> 
				</div>
			</Form>
			
			
		</div>
	)
}


