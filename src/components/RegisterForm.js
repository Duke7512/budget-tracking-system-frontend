import { Form, Button, Modal } from 'react-bootstrap';
import { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types'


const RegisterForm = props => {
	const {show, handleClose} = props;

	const history = useHistory()

  	

	const [ credentials, setCredentials ] = useState({
		firstName: "",
		lastName: "",
		email: "",
		password: "",
		confirmPassword: ""
	})

	const [ isLoading, setIsLoading ] =useState(false)

	const handleSubmit = e =>{
		e.preventDefault()
		setIsLoading(true)
		fetch('https://quiet-hollows-90585.herokuapp.com/api/users/register',{
			method: "POST",
			body: JSON.stringify(credentials),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then( res => {
			console.log(res)
			setIsLoading(false)
			if (credentials.password !== credentials.confirmPassword) {
				alert("Passwords doesn't match")
			
			} else {
				if (res.status === 200){
					alert("Register Succesful")
					history.push("/")
					return res.json()
				} else {

				}
			}
		})
		
	}

	const handleChange = e =>{
		setCredentials({
			...credentials,
			[e.target.id] : e.target.value
		})
	}


	return(
		<>

			{/* <Button variant="primary" onClick={handleShow}>
        	Launch demo modal
      		</Button> */}

			<Modal show={show} onHide={handleClose} animation={false}>
			<Modal.Header closeButton>
			<Modal.Title>Registration</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form onSubmit={handleSubmit}>

					<Form.Group controlId="firstName">
						<Form.Label>First Name:</Form.Label>
						<Form.Control 
							type="text"
							onChange={handleChange}
							value={credentials.firstName}
						/>
					</Form.Group>

					<Form.Group controlId="lastName">
						<Form.Label>Last Name:</Form.Label>
						<Form.Control 
							type="text" 
							onChange={handleChange}
							value={credentials.lastName}
						/>
					</Form.Group>

					<Form.Group controlId="email">
						<Form.Label>Email:</Form.Label>
						<Form.Control 
							type="email" 
							onChange={handleChange}
							value={credentials.email}
						/>
					</Form.Group>

					<Form.Group controlId="password">
						<Form.Label>Password:</Form.Label>
						<Form.Control 
							type="password" 
							onChange={handleChange}
							value={credentials.password}
						/>
					</Form.Group>

					<Form.Group controlId="confirmPassword">
						<Form.Label>Confirm Password:</Form.Label>
						<Form.Control 
							type="password" 
							onChange={handleChange}
							value={credentials.confirmPassword}
						/>
					</Form.Group>
					{/* { 	isLoading ?
							<Button type="submit" disabled>Submit</Button>
						:   <Button type="submit" >Submit</Button>
					} */}
				</Form>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="secondary" onClick={handleClose}>
					Close
				</Button>
				<Button variant="primary" onClick={handleSubmit}>
					Submit
				</Button>
        	</Modal.Footer>
      		</Modal>
    	</>
		
	)
}
RegisterForm.propType = {
	show: PropTypes.bool, 
	setShow: PropTypes.func
}
export default RegisterForm;

