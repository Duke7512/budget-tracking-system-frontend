import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import MainNav from './components/MainNav';
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Income from './pages/Income'
import Expense from './pages/Expense'
import { 
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import ApplicationProvider from './contexts/ApplicationContext'


function App() {
  return (
    <div className="App">
      <ApplicationProvider>
        <Router>
          <MainNav />

          <Switch>

            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <Route path="/income">
              <Income />
            </Route>
            <Route path="/expense">
              <Expense />
            </Route>

          </Switch>

        </Router>
      </ApplicationProvider>
    </div>
  );
}

export default App;
