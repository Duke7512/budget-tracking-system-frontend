import { useEffect, useState } from "react";
import '../App.css';
import { Container, Row, Col } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import LoginForm from './../components/LoginForm'

export default function Home() {

    const [isRedirect, setIsRedirect ] = useState(false)
    
	return (
        <div className="Home">
		    {isRedirect ?		
		    <Redirect to="/" /> :
		    <Container>
			    <Row>
				    <Col  xs={12} sm={10} md={5}>
					    <LoginForm setIsRedirect={setIsRedirect} />
				    </Col>
			    </Row>
		    </Container>}
        </div>
	)
}
