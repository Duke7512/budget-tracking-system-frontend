import { useEffect, useState } from "react";
import { Form, Button, Table, Row, Col, Container } from 'react-bootstrap';

export default function Income(){

// get All categ
    const [expenseCategories, setExpenseCategories] = useState([])

    const [isLoading, setIsLoading] = useState(false)

    useEffect( () => {
        fetch('https://quiet-hollows-90585.herokuapp.com/api/users', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            getCategories()
            return data
        })
}, []);



    const getCategories = () => {
        setIsLoading(false)
        fetch('https://quiet-hollows-90585.herokuapp.com/api/transactions/expenseCategory', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`   
            }
        })
        .then( res => {
            setIsLoading(true)
           return res.json()
        })
        .then( data => {
            setExpenseCategories(data)
        })
    }

    useEffect(() => {
        if(isLoading){
        getCategories()
        }
    }, [isLoading])

// add
    const [expenseCategory, setExpenseCategory] = useState({
        category: ""
    })


    const handleSubmit = (e) => {
        e.preventDefault()
        fetch('https://quiet-hollows-90585.herokuapp.com/api/transactions/expenseCategory',{
            method: "POST",
            body: JSON.stringify(expenseCategory),
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}` 
            }
        })
        .then( res => {
            expenseCategory.category = "";
            return res.json()
        })
        .then(data => {
            console.log(data)
        })
        .catch( err => console.log(err))
    }

    const handleChange = (e)=> {
        setExpenseCategory({
            [e.target.id]: e.target.value
        })
    }

    // get expense

    const [expenses, setExpenses] = useState([])

    useEffect( () => {
        fetch('https://quiet-hollows-90585.herokuapp.com/api/users', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            
                getExpense()
                return data
            })
    }, []);

     const getExpense = () => {
        fetch('https://quiet-hollows-90585.herokuapp.com/api/transactions/expense', {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`   
                }
            })
            .then( res => res.json())
            .then( data => {
                setExpenses(data)
            })
    }




    const [mode, setMode] = useState("create")

    const [newCategory, setNewCategory] = useState("")

    const [categoryId, setCategoryId] = useState("")

    const updateCategory = (e)=> {
        e.preventDefault();
        setNewCategory(e.target.value)
    }

    const editCategory = (e, category, id)=> {
        e.preventDefault();
        setNewCategory(category)
        setCategoryId(id)
        setMode("update")
    }

    const saveCategory = (e) => {
        e.preventDefault()

        fetch(`https://quiet-hollows-90585.herokuapp.com/api/transactions/expenseCategory/${categoryId}`, {
            method : "PUT",
            body: JSON.stringify({"category": newCategory}),
            headers: {
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => {
            setNewCategory("")
            return res.json()
        })
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

     const deleteCategory = (e, id) => {
        e.preventDefault();
        
        fetch(`https://quiet-hollows-90585.herokuapp.com/api/transactions/expenseCategory/${id}`, {
            method : "DELETE",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }


    const [entryCategoryId, setEntryCategoryId] = useState("")

    const [entryAmount, setEntryAmount] = useState("")

    const [newEntryAmount, setNewEntryAmount] = useState("")

//     const [newEntryCategory, setNewEntryCategory] = useState("")

    const [newCategoryId, setNewCategoryId] = useState("")

    const [oldCategoryId, setOldCategoryId] = useState("")

    const [incomeId, setIncomeId] = useState("")

    const editEntry = (e, category, id, amount, entryCategoryId)=> {
        e.preventDefault();
        setNewEntryAmount(amount)
        setOldCategoryId(entryCategoryId)
        setIncomeId(id)
    }

    const setSelectedValues = (e)=> {
        e.preventDefault();
        setEntryCategoryId(e.target.value)
        setNewCategoryId(e.target.value)
    }

// // add expense
    const createEntry = (e) => {
        e.preventDefault()
        setIsLoading(false)
        fetch(`https://quiet-hollows-90585.herokuapp.com/api/transactions/${entryCategoryId}/expense`, {
            method: "POST",
            body: JSON.stringify({"amount": entryAmount}),
            headers:{
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            setIsLoading(true)
            setEntryAmount("")
           return res.json()

        })
        .catch( err => console.log(err));
    }

    useEffect(() => {
        if(isLoading){
            getExpense()
        }
    }, [isLoading])

    const updateEntry = (e) => {
        e.preventDefault()

        fetch(`https://quiet-hollows-90585.herokuapp.com/api/transactions/${oldCategoryId}/expense/${incomeId}`, {
            method : "PUT",
            body: JSON.stringify({"expenseCategoryId": {"_id": newCategoryId}, "amount": newEntryAmount}),
            headers: {
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => {
            setIsLoading(true)
            setNewEntryAmount("")
            return res.json()
        })
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))

    }


// delete expense   
    const deleteEntry = (e, categoryId, id ) => {
        e.preventDefault();
        setIsLoading(false)
        
        fetch(`https://quiet-hollows-90585.herokuapp.com/api/transactions/${categoryId}/expense/${id}`, {
            method : "DELETE",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => {
            setIsLoading(true)
            return res.json()
        })
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }


    return(
        <div>
        <Container>
                <Row>
                    <Col>
                        <Form onSubmit={handleSubmit}>

                            <Form.Group controlId="category">
                                <Form.Label>Category:</Form.Label>
                                <Form.Control 
                                    type="text"
                                    onChange={handleChange}
                                    value={expenseCategory.category}
                                />
                            </Form.Group>

                              <Button type="submit" >Add</Button>
                            
                        </Form>
                    </Col>
                    <Col>
                        <Form onSubmit={saveCategory}>

                            <Form.Group controlId="category">
                                <Form.Label>Category:</Form.Label>
                                <Form.Control 
                                    type="text"
                                    onChange={updateCategory}
                                    value={newCategory}
                                />
                            </Form.Group>

                        <Button type="submit" >Update</Button>
                        
                        </Form>
                    </Col>
                    <Col>
                        <Form onSubmit={createEntry}>

                            <Form.Group controlId="category">
                                 <Form.Label>Category: </Form.Label>
                                    <Form.Control size="sm" as="select" onChange={setSelectedValues}>
                                        <option></option>
                                        {expenseCategories?.map(expense => {
                                            return (
                                                <option value={expense._id}>{expense.category}</option>
                                            )
                                        })
                                        }
                                    </Form.Control>

                                <Form.Label>Amount</Form.Label>
                                <Form.Control 
                                    type="text"
                                    onChange={(event)=>setEntryAmount(event.target.value)}
                                    value={entryAmount}
                                />
                            </Form.Group>

                            <Button type="submit" >Add</Button>
                            
                        </Form>
                    </Col>
                    <Col>
                        <Form onSubmit={updateEntry}>

                            <Form.Group controlId="category">
                                 <Form.Label>Category: </Form.Label>
                                    <Form.Control size="sm" as="select" onChange={setSelectedValues}>
                                        <option></option>
                                        {expenseCategories?.map(expense => {
                                            return (
                                                <option value={expense._id}>{expense.category}</option>
                                            )
                                        })
                                        }
                                    </Form.Control>

                                <Form.Label>Amount</Form.Label>
                                <Form.Control 
                                    type="text"
                                    onChange={(event)=>setNewEntryAmount(event.target.value)}
                                    value={newEntryAmount}
                                />
                            </Form.Group>

                            <Button type="submit" >Update</Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        
            <Container>
                <Row>
                    <Col>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Value</th>
                                </tr>
                            </thead>

                            <tbody>
        {expenseCategories?.map(iC => {

              return (
                    
                            <tr key={iC._id}>
                                <td>{iC.category}</td>
                                <Button variant="warning" onClick={(event) => editCategory(event, iC.category, iC._id)}>Edit</Button>
                                <Button variant="danger" onClick={(event) => deleteCategory(event, iC._id)}>Delete</Button>
                            </tr>
                        
                       
                 )
             })}
                          </tbody>
                        </Table>  
                    </Col>
                    {<Col>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
        {expenses?.map(i => {

              return (
                    
                            <tr key={i._id}>
                                {<td>{i.expenseCategoryId.category}</td>}
                                <td>{i.amount}</td>
                                <Button variant="warning" onClick={(event) => editEntry(event, i.category, i._id, i.amount, i.expenseCategoryId._id)}>Edit</Button>
                                <Button variant="danger" onClick={(event) => deleteEntry(event, i.expenseCategoryId._id, i._id)}>Delete</Button>
                            </tr>
                        
                       
                 )
             })}
                          </tbody>
                        </Table>
                    </Col>}
                </Row> 
            </Container>       
        </div>
    )
}
