import RegisterForm from './../components/RegisterForm'
import { Form, Button, Container,Row, Col } from 'react-bootstrap'

export default function Register() {
  return (
    <Container>
      <Row>
      	<Col className="mx-auto col-12" style={{ marginTop: "15%" }}>
			<RegisterForm />
		</Col>
      </Row>
    </Container>
  )
}